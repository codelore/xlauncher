package ru.pixelsky.xlauncher.theme;

import java.awt.Color;

import javax.swing.border.EmptyBorder;
import ru.pixelsky.xlauncher.components.Align;
import ru.pixelsky.xlauncher.components.ButtonStyle;
import ru.pixelsky.xlauncher.components.CheckboxStyle;
import ru.pixelsky.xlauncher.components.ComboboxStyle;
import ru.pixelsky.xlauncher.components.DragbuttonStyle;
import ru.pixelsky.xlauncher.components.DraggerStyle;
import ru.pixelsky.xlauncher.components.PassfieldStyle;
import ru.pixelsky.xlauncher.components.ServerbarStyle;
import ru.pixelsky.xlauncher.components.TextfieldStyle;



public class LoginTheme
{
	public static int			 frameW 	= 371;
	public static int			 frameH		= 510; 

	public static ButtonStyle	 toGame		= new ButtonStyle(30, 389, 150, 40, "font", "button", 0F, Color.DARK_GRAY, true, Align.CENTER);
	public static ButtonStyle	 toPersonal = new ButtonStyle(0, 0, 0, 0, "font", "button", 16F, Color.DARK_GRAY, false, Align.CENTER);
	public static ButtonStyle	 toOptions  = new ButtonStyle(190, 389, 150, 40, "font", "button_settings", 0F, Color.DARK_GRAY, true, Align.CENTER);
	
	public static CheckboxStyle  savePass   = new CheckboxStyle(30, 453, 260, 24, "font", "checkbox", 16F, Color.WHITE, true);
	public static TextfieldStyle login		= new TextfieldStyle(30, 149, 310, 34, "textfield", "font", 16F, TextColor.Black, Color.BLACK, new EmptyBorder(0, 10, 0, 10));
	public static PassfieldStyle password	= new PassfieldStyle(30, 229, 310, 34, "textfield", "font", 16F, TextColor.Black, Color.DARK_GRAY, "*", new EmptyBorder(0, 10, 0, 10));

	public static DragbuttonStyle dbuttons	= new DragbuttonStyle(240, 0, 59, 24, 300, 0, 59, 24, "draggbutton", true);
	public static DraggerStyle	  dragger	= new DraggerStyle(0, 0, 732, 24, "font", 19F, Color.WHITE, Align.LEFT);
	
	public static ComboboxStyle	 servers	= new ComboboxStyle(30, 309, 310, 34, "font", "combobox", 14F, TextColor.Black, true, Align.CENTER);
	public static ServerbarStyle serverbar	= new ServerbarStyle(30, 338, 310, 16, "font", 14F, Color.WHITE, true);
	
	public static float fontbasesize		= 16F;
	public static float fonttitlesize		= 24F;
}