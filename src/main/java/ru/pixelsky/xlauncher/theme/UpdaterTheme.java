package ru.pixelsky.xlauncher.theme;

import java.awt.Color;

public class UpdaterTheme
{
	public static int	loadbarX = 10;
	public static int	loadbarY = 450;
	public static int	loadbarW = 351;
	public static int	loadbarH = 38;

	public static int	stringsX = 40;
	public static int	stringsY = 180;
	
	public static FontBundle updaterDesc	=	new FontBundle("font", 17F, Color.WHITE);
}