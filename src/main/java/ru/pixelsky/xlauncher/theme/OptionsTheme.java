package ru.pixelsky.xlauncher.theme;

import java.awt.Color;
import javax.swing.border.EmptyBorder;

import ru.pixelsky.xlauncher.components.Align;
import ru.pixelsky.xlauncher.components.ButtonStyle;
import ru.pixelsky.xlauncher.components.CheckboxStyle;
import ru.pixelsky.xlauncher.components.ComponentStyle;
import ru.pixelsky.xlauncher.components.TextfieldStyle;


public class OptionsTheme
{	
	public static ComponentStyle	panelOpt	= new ComponentStyle(225, 105, 400, 300, "font", 16F, Color.DARK_GRAY, true);
	
//	public static CheckboxStyle		loadnews	= new CheckboxStyle(250, 150, 300, 23, "font", "checkbox", 16F, TextColor.Black, true);
	public static CheckboxStyle		updatepr	= new CheckboxStyle(40, 175, 300, 23, "font", "checkbox", 16F, TextColor.Black, true);
	public static CheckboxStyle		cleandir	= new CheckboxStyle(40, 200, 300, 23, "font", "checkbox", 16F, TextColor.Black, true);
	public static CheckboxStyle		fullscrn	= new CheckboxStyle(40, 225, 300, 23, "font", "checkbox", 16F, TextColor.Black, true);
	public static CheckboxStyle		offline		= new CheckboxStyle(40, 250, 300, 23, "font", "checkbox", 16F, TextColor.Black, true);
	public static TextfieldStyle	        memory		= new TextfieldStyle(40, 364, 250, 36, "textfield", "font", 16F, TextColor.Black, TextColor.Black, new EmptyBorder(0, 10, 0, 10));
	public static ButtonStyle		close		= new ButtonStyle	(130, 400, 119, 40, "font", "button_close", 0F, TextColor.Black, true, Align.CENTER);
	
	public static FontBundle		memoryDesc	= new FontBundle("font", 16F, TextColor.Black);
	
	public static int titleX 		= 100;
	public static int titleY 		= 100;
}