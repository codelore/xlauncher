package ru.pixelsky.xlauncher.utils;

import java.awt.Dimension;

import ru.pixelsky.xlauncher.components.Frame;
import static ru.pixelsky.xlauncher.theme.LoginTheme.*;
import static ru.pixelsky.xlauncher.theme.OptionsTheme.*;

public class ThemeUtils extends BaseUtils
{
    public static void updateStyle(Frame main) throws Exception
    {
	dragger.apply(main.dragger);
	dbuttons.apply(main.hide, main.close);
	toGame.apply(main.toGame);
	toOptions.apply(main.toOptions);
	savePass.apply(main.savePass);
	login.apply(main.login);
	password.apply(main.password);
	servers.apply(main.servers);
	serverbar.apply(main.serverbar);
		
	updatepr.apply(main.updatepr);
	cleandir.apply(main.cleanDir);
	fullscrn.apply(main.fullscreen);
	offline.apply(main.offline);
	memory.apply(main.memory);
	close.apply(main.options_close);
		
	main.panel.setPreferredSize(new Dimension(frameW, frameH));
		
	main.setIconImage(BaseUtils.getLocalImage("favicon"));
	main.setLocationRelativeTo(null);
	main.pack();
	main.repaint();
    }
}