package ru.pixelsky.xlauncher.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import ru.pixelsky.xlauncher.components.Frame;
import ru.pixelsky.xlauncher.components.Game;
import ru.pixelsky.xlauncher.run.Settings;

public class ThreadUtils
{
    public static String b =  "bin";
    public static String l =  "libraries.jar";
    public static String f =  "Forge.jar";
    public static String e =  "extra.jar";
    public static String m =  "minecraft.jar";
    public static UpdaterThread updaterThread;
    public static Thread serverPollThread;
        
    public static void auth()
    {
        if(Frame.main.offline.isSelected())
	{
            new Game(null);
            return;
	}
	BaseUtils.send("Logging in, login: " + Frame.main.login.getText());
	Thread t = new Thread() {
            public void run()
            {
                try 
                {
                    String answer = BaseUtils.execute(BaseUtils.buildUrl("launcher.php"), 
                            new Object[]
                            {
                                "action", "auth",
				"client", BaseUtils.getClientName(),
				"login", Frame.main.login.getText(),
				"password", new String(Frame.main.password.getPassword()),
                            });
		boolean error = false;
		if(answer == null)
		{
                    Frame.main.panel.tmpString = "Ошибка подключения (Нет подключения к сети)";
                    error = true;
		}
                else if(answer.contains("errorLogin"))
		{
                    Frame.main.panel.tmpString = "Ошибка авторизации (Логин, пароль)";
                    error = true;
		}
                else if(answer.contains("badhash"))
		{
                    Frame.main.panel.tmpString = "Ошибка: Неподдерживаемый способ шифровки";
                    error = true;	
		}
                else if(answer.split("<br>").length != 3)
		{
                    Frame.main.panel.tmpString = answer;
                    error = true;
		}
                if(error)
		{
                    Frame.main.panel.tmpColor = Color.red;
                    try
                    {
                        sleep(2000);
                    } 
                    catch (InterruptedException e) {}
                    Frame.main.setAuthComp();
		}
                else
		{
                    String version = answer.split("<br>")[0].split("<:>")[6];	
                    if(!version.equals(Settings.masterVersion))
                    {
                        Frame.main.setLauncherUpdateState();
                        Frame.main.updateLauncher();
			return;
                    }
                    BaseUtils.send("Logging in successful");
                    runUpdater(answer);
		}
                    interrupt();
                }
                catch(Exception e){ e.printStackTrace(); }
            }};
	t.setName("Auth thread");
		t.start();	
    }
    
    public static void runUpdater(String answer)
    {
        boolean zipupdate = false;
	boolean zipupdate2 = false;
	List<String> files = GuardUtils.updateMods_first(answer);
	String binfolder = BaseUtils.getMcDir() + File.separator + b + File.separator;
	String folder = BaseUtils.getAssetsDir() + File.separator;
        
	if(!EncodingUtils.xorencode(EncodingUtils.inttostr(answer.split("<br>")[0].split("<:>")[0]), Settings.protectionKey).equals(BaseUtils.getPropertyString(BaseUtils.getClientName() + "_zipmd5")) ||
            !new File(binfolder + "natives").exists() || Frame.main.updatepr.isSelected()) { files.add(b+"/client.zip");  zipupdate = true; }
		
	int i = Integer.parseInt(Settings.servers[Frame.main.servers.getSelectedIndex()].split(", ")[4]);
	if (i > 1)
	{
            if(!EncodingUtils.xorencode(EncodingUtils.inttostr(answer.split("<br>")[0].split("<:>")[1]), Settings.protectionKey).equals(BaseUtils.getPropertyString("assetsmd5")) ||
                !new File(folder + "assets").exists() || Frame.main.updatepr.isSelected()) { files.add(b+"/assets.zip");  zipupdate2 = true; }
        }
	
        if(!EncodingUtils.xorencode(EncodingUtils.inttostr(answer.split("<br>")[0].split("<:>")[3]), Settings.protectionKey).equals(GuardUtils.getMD5(binfolder + l))) files.add(b+"/"+l);
	if(!EncodingUtils.xorencode(EncodingUtils.inttostr(answer.split("<br>")[0].split("<:>")[4]), Settings.protectionKey).equals(GuardUtils.getMD5(binfolder + f))) files.add(b+"/"+f);
	if(!EncodingUtils.xorencode(EncodingUtils.inttostr(answer.split("<br>")[0].split("<:>")[5]), Settings.protectionKey).equals(GuardUtils.getMD5(binfolder + e))) files.add(b+"/"+e);
	if(!EncodingUtils.xorencode(EncodingUtils.inttostr(answer.split("<br>")[0].split("<:>")[2]), Settings.protectionKey).equals(GuardUtils.getMD5(binfolder + m))) files.add(b+"/"+m);	
        
        BaseUtils.send("---- Filelist start ----");
	for(Object s : files.toArray())
	{
            BaseUtils.send("- " + (String) s);
	}
	BaseUtils.send("---- Filelist end ----");
	BaseUtils.send("Running updater...");
	updaterThread = new UpdaterThread(files, zipupdate, zipupdate2, answer);
	updaterThread.setName("Updater thread");
	Frame.main.setUpdateState();
	updaterThread.run();
    }
	
    public static void pollSelectedServer()
    {
        try
	{
            serverPollThread.interrupt();
            serverPollThread = null;
	}
        catch (Exception e) {}
		
	BaseUtils.send("Refreshing server state... (" + Frame.main.servers.getSelected() + ")");
	serverPollThread = new Thread()
	{
            public void run()
            {
                Frame.main.serverbar.updateBar("Обновление...", BaseUtils.genServerIcon(new String[]{null, "0", null}));
                int sindex = Frame.main.servers.getSelectedIndex();
                String ip = Settings.servers[sindex].split(", ")[1];
		int port = BaseUtils.parseInt(Settings.servers[sindex].split(", ")[2], 25565);		
				
                if(Frame.main.offline.isSelected())
		{
                    Frame.main.serverbar.updateBar("Выбран оффлайн", BaseUtils.genServerIcon(new String[]{null, "0", null}));
                    return;
		}
				
                String[] status = BaseUtils.pollServer(ip, port);
                String text = BaseUtils.genServerStatus(status);
                BufferedImage img = BaseUtils.genServerIcon(status);
		Frame.main.serverbar.updateBar(text, img);
				
		serverPollThread.interrupt();
                serverPollThread = null;
		BaseUtils.send("Refreshing server done!");
            }
	};
	serverPollThread.setName("Server poll thread");
	serverPollThread.start();
    }
    
    public static final String[] updateServers(String path)
    {    
        try
        {
            String[] site = BaseUtils.openURL(path).split(":");
            Settings.servers = new String[site.length];
            Settings.servers = BaseUtils.openURL(path).split(":");
            String[] serverNames = new String[Settings.servers.length];
                    
            for(int i = 0; i < Settings.servers.length; i++)
            {
                serverNames[i] = Settings.servers[i].split(", ")[0];
            }
            
            String serversToOff = BaseUtils.openURL(path);
            write(BaseUtils.getPixelSkyServersFile(), serversToOff);
            
            return serverNames;
          }
        catch(Exception e)
        {
            String[] s = null;
            
            try
            {
                s = read(BaseUtils.getPixelSkyServersFile()).split(":");
            }
            catch (Exception ex)
            {
                BaseUtils.sendErr("Error servers writing.");
                return null;
            }
            
            Settings.servers = new String[s.length];
            
            try
            {
                Settings.servers = s;
            }
            catch (Exception ex)
            {
                BaseUtils.sendErr("Error read servers.");
                return null;
            }
            String[] serverNames = new String[Settings.servers.length];
                    
            for(int i = 0; i < Settings.servers.length; i++)
            {
                serverNames[i] = Settings.servers[i].split(", ")[0];
            }
            return serverNames;
        }
    }
    
    public static void write(File f, String text)
    {
        try
        {
            if(!f.exists())
            {
                f.createNewFile();
            }
          
            PrintWriter out = new PrintWriter(f.getAbsoluteFile());
            try
            {
                out.print(text);
            }
            finally
            {
                out.close();
            }
        }
        catch(IOException e) 
        {
            throw new RuntimeException(e);
        }
    }
       
    public static String read(File file) throws FileNotFoundException 
    {      
        StringBuilder sb = new StringBuilder();
 
        exists(file.getAbsolutePath());
 
        try
        {
            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try
            {
                String s;
                while ((s = in.readLine()) != null) 
                {
                    sb.append(s);
                }
            }
            finally
            {
                in.close();
            }
        }
        catch(IOException e)
        {
            throw new RuntimeException(e);
        }
            
        return sb.toString();
    }
    
    private static void exists(String fileName) throws FileNotFoundException 
    {
        File file = new File(fileName);
        if (!file.exists())
        {
            throw new FileNotFoundException(file.getName());
        }
    }
}