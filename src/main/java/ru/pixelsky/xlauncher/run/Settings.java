/*launcher, сборка за 30.10.2012, индекс: 121 */

//Добавлена регистрация под webmcr (md5) в тестовом режиме, ставить на боевом сайте крайне не рекомендуется

package ru.pixelsky.xlauncher.run;

public class Settings
{
	/** Настройка заголовка лаунчера */
	public static final String 		title		 	 = "PixelSky Launcher"; //Заголовок лаунчера
	public static final String 		titleInGame  	 = "PixelSky Client"; //Заголовок лаунчера после авторизации
	public static final String 		basedir			 = "AppData"; //Родительская папка для Minecraft (только для Windows) [ProgramFiles, AppData]
	public static final String 		baseconf		 = ".pixelsky"; //Папка с файлом конфигурации
	public static final String		pathconst		 = ".pixelsky/%SERVERNAME%"; //Конструктор пути к папке с MC
	public static final String        skins            = "skins/"; //Папка скинов
	public static final String        cloaks           = "cloaks/"; //Папка плащей
	/** Параметры подключения */
	public static final String 	domain	 	 	 = "c.pixelsky.ru";//Домен сайта
	public static final String  siteDir		  	 = "auth";//Папка с файлами лаунчера на сайте
	public static final String  updateFile		 = "http://c.pixelsky.ru/update/PixelSky.exe";//Ссылка на файл обновления лаунчера
	public static final String 	buyVauncherLink  = ""; //Ссылка на страницу покупки ваучеров
	
	/** Для одиночной игры */
	public static final String  defaultUsername  = "player"; //Имя пользователя для одиночной игры
	public static final String  defaultSession   = "123456"; //Номер сессии для одиночной игры
	
	/** Настройка серверов */
	// 1-> Имя папки клиента 2-> ip 3-> port 
	// 4-> Версия клиента для автопатча директории (старые версии до 1.5.2)
	// 5-> Тип запуска клиента 1 для старых версий 2 для новых
	// 6-> 1 для запуска чистого клиента
	//     2 для запуска клиента с forge без Liteloader
	//     3 для запуска клиента с Liteloader и Liteloader+forge
	
        public static String[] servers =
	{
		"Sandbox, s1.pixelsky.ru, 25565, 1.5.x, 1, 3"
	};

	/** Настройки структуры лаунчера */
	public static boolean useAutoenter			 =  false; //Использовать функцию автозахода на выбранный сервер
	
	public static boolean useRegister		 	 =  false; //Использовать Регистрацию в лаунчере
	
	public static boolean useMulticlient		 =  true; //Использовать функцию "по клиенту на сервер"
	public static boolean useStandartWB			 =  true; //Использовать стандартный браузер для открытия ссылок
	public static boolean customframe 			 =  true; //Использовать кастомный фрейм
	public static boolean useOffline 			 =  true; //Использовать режим оффлайн
	public static boolean useConsoleHider		 =  true; //Использовать скрытие консоли клиента
	public static boolean useModCheckerTimer	 =  true; //Каждые 30 секунд моды будут перепроверяться

	public static final String protectionKey	 = "pixelskyforever"; //Ключ защиты сессии. Никому его не говорите.

	public static final boolean debug		 	 =  false;  //Отображать все действия лаунчера (отладка)(true/false)
	public static final boolean drawTracers		 =  false; //Отрисовывать границы элементов лаунчера
	public static final String masterVersion  	 = "6"; //Версия лаунчера

	public static final boolean patchDir 		 =  true; //Использовать автоматическую замену директории игры (true/false)
 	
        public static void onStartMinecraft() {}
}
