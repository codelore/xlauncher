package ru.pixelsky.xlauncher.components;

public enum Align
{
    LEFT, CENTER, RIGHT
}