package ru.pixelsky.xlauncher.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import ru.pixelsky.xlauncher.run.Settings;
import ru.pixelsky.xlauncher.utils.BaseUtils;
import ru.pixelsky.xlauncher.utils.EncodingUtils;
import ru.pixelsky.xlauncher.utils.ImageUtils;
import ru.pixelsky.xlauncher.utils.ThemeUtils;
import ru.pixelsky.xlauncher.utils.ThreadUtils;

import static ru.pixelsky.xlauncher.utils.BaseUtils.*;

public class Frame extends JFrame implements ActionListener, FocusListener
{
	private static final long serialVersionUID = 1L;
	public static Frame main;
        public Panel panel = new Panel(0);
	public Dragger dragger = new Dragger();
	public Button toGame = new Button("Играть");
	public Button toOptions = new Button("Настройки");
        public Button toRegister = new Button("Регистрация");
	public Checkbox savePass = new Checkbox("Сохранить пароль");
	public JTextPane browser = new JTextPane();
	public JTextPane personalBrowser = new JTextPane();
	public JScrollPane personalBpane = new JScrollPane(personalBrowser);
	public Textfield login = new Textfield();
	public Passfield password = new Passfield();
	public Combobox servers = new Combobox(ThreadUtils.updateServers("http://c.pixelsky.ru/servers.txt"), 0);
	public Serverbar serverbar = new Serverbar();

	public Dragbutton hide = new Dragbutton();
	public Dragbutton close = new Dragbutton();

	public Button update_yes = new Button("Обновить");

	public Checkbox updatepr = new Checkbox("Обновить");
	public Checkbox cleanDir = new Checkbox("Очистить папку");
	public Checkbox fullscreen = new Checkbox("Полный экран");
	public Textfield memory = new Textfield();
                        
        public Button options_close = new Button("Закрыть");

	public Button toGamePersonal = new Button("В игру");

	public Checkbox offline = new Checkbox("Режим оффлайн");

	public Frame()
	{	
		//Подготовка окна
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setBackground(Color.DARK_GRAY);
            setForeground(Color.DARK_GRAY);
            setLayout(new BorderLayout());

            setUndecoratdIfPossible();

            setResizable(false);
            try
            {
		ThemeUtils.updateStyle(this);
            } 
            catch (Exception e)
            {
                e.printStackTrace();
            }

            //Добавление слушателей
            toGame.addActionListener(this);
            toOptions.addActionListener(this);
            toRegister.addActionListener(this);
            toRegister.setVisible(Settings.useRegister);
            login.setText("Логин...");
            login.addActionListener(this);
            login.addFocusListener(this);
            password.setText("itspassword");
            password.setEchoChar('*');
            password.addActionListener(this);
            password.addFocusListener(this);

            personalBpane.setOpaque(false);
            personalBpane.getViewport().setOpaque(false);
            personalBpane.setBorder(null);

            personalBrowser.setOpaque(false);
            personalBrowser.setBorder(null);
            personalBrowser.setContentType("text/html");
            personalBrowser.setEditable(false);
            personalBrowser.setFocusable(false);
          
            browser.setOpaque(false);
            browser.setBorder(null);
            browser.setContentType("text/html");
            browser.setEditable(false);
            browser.setFocusable(false);
            hide.addActionListener(this);
            close.addActionListener(this);
                
            servers.addMouseListener(
                    new MouseListener()
                    {
                        public void mouseReleased(MouseEvent e){}
                        public void mousePressed(MouseEvent e) {}
                        public void mouseExited(MouseEvent e)  {}
                        public void mouseEntered(MouseEvent e) {}
                        public void mouseClicked(MouseEvent e)
                        {
                            if(servers.getPressed() || e.getButton() != MouseEvent.BUTTON1) return;

                            ThreadUtils.pollSelectedServer();
                            setProperty("server", servers.getSelectedIndex());
                        }
                    });

            options_close.addActionListener(this);
            fullscreen.addActionListener(this);
            toGamePersonal.addActionListener(this);

            login.setText(getPropertyString("login"));
            String pass = getPropertyString("password");
            password.setText(pass == null ? "itspassword" : EncodingUtils.decode(pass));
            savePass.setSelected(pass != null);
            servers.setSelectedIndex(getPropertyInt("server"));
            addAuthComp();
            addFrameComp();
            add(panel, BorderLayout.CENTER);

            pack();
            setLocationRelativeTo(null);
            validate();
            repaint();
            setVisible(true);
        }

    private void setUndecoratdIfPossible() 
    {
        try
        {
            setUndecorated(Settings.customframe);
        }
        catch (Exception ignore) {}
    }

    public void addFrameComp()
    {
        if(Settings.customframe)
	{
            panel.add(hide);
            panel.add(close);
            panel.add(dragger);
	}
    }

    public void setAuthComp()
    {
	panel.type = 0;
	panel.timer.stop();
	panel.removeAll();
	addFrameComp();
	addAuthComp();
	repaint();
    }

    /** Добавление элементов авторизации*/
    public void addAuthComp()
    {
	panel.add(servers);
	panel.add(serverbar);
	panel.add(toGame);
	panel.add(toOptions);
        panel.add(login);
	panel.add(password);
	panel.add(savePass);
    }

    //Старт программы
    public static void start()
    {
        try
        {
            send("PixelSky (c).");
            try
            {
                send("Setting new LaF...");
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } 
            catch(Exception e)
            {
                send("Fail setting LaF");
            }
            send("Running debug methods...");

            new Runnable()
            {
                public void run()
		{
                    if(!BaseUtils.tryLock(BaseUtils.getPixelSkyConfigName()))
                    {
                        System.exit(0);
                    }
                                        
		}
            }.run();

            main = new Frame();

            ThreadUtils.pollSelectedServer();
            try
            {
                main.memory.setText(String.valueOf(getPropertyInt("memory", 512)));
                main.fullscreen.setSelected(getPropertyBoolean("fullscreen"));
            }
            catch(Exception e){}
	}
        catch(Exception e)
	{
            throwException(e, main);
	}
    }
    
    @SuppressWarnings("deprecation")
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == hide) setExtendedState(ICONIFIED);
	if(e.getSource() == close) System.exit(0);

        if(e.getSource() == login || e.getSource() == password || e.getSource() == toGame || e.getSource() == toGamePersonal)
	{
            setProperty("login", login.getText());
            setProperty("server", servers.getSelectedIndex());
            if(savePass.isSelected()) setProperty("password", EncodingUtils.encode(new String(password.getPassword())));
            else setProperty("password", empty);
            panel.remove(hide);
            panel.remove(close);
            BufferedImage screen = ImageUtils.sceenComponent(panel);
            panel.removeAll();
            addFrameComp();
            panel.setAuthState(screen);
            ThreadUtils.auth();
	}

	if(e.getSource() == toOptions)
	{
            setOptions();
	}            
                
	if(e.getSource() == options_close)
	{
            if(!memory.getText().equals(getPropertyString("memory")))
            {
                try
		{
                    int i = Integer.parseInt(memory.getText());
                    setProperty("memory", i);
		}
                catch(Exception e1){}
                try
                {
                    restart();
                }
                catch(Exception ex)
                {
                    ex.getLocalizedMessage(); 
                }
            }
            setAuthComp();
	}

	if(e.getSource() == fullscreen || e.getSource() == offline)
	{
            setProperty("fullscreen", fullscreen.isSelected());
            setProperty("offline",    offline.isSelected());
	}
    }

    public void focusGained(FocusEvent e)
    {
        if(e.getSource() == login && login.getText().equals("Логин...")) login.setText(empty);
	if(e.getSource() == password && new String(password.getPassword()).equals("itspassword")) password.setText(empty);
    }

    public void focusLost(FocusEvent e)
    {
        if(e.getSource() == login && login.getText().equals(empty)) login.setText("Логин...");
	if(e.getSource() == password && new String(password.getPassword()).equals(empty)) password.setText("itspassword");
    }
    
    public void setLauncherUpdateState()
    {
        BufferedImage screen = ImageUtils.sceenComponent(panel);
        panel.removeAll();
        addFrameComp();
        panel.setLoadingState(screen, "Обновление лаунчера...");   
    }
    
    public void updateLauncher() 
    {
            new Thread()
            {
                public void run() 
                { 
                    try
                    {
                        BaseUtils.updateLauncher();
                    } 
                    catch(Exception e1)
                    {
                        e1.printStackTrace();
                        send("Error updating launcher!");
                        panel.type = 9;
			panel.repaint();
                    }
                }
            }.start();
    }

    public void setUpdateState()
    {
        panel.removeAll();
        addFrameComp();
	panel.setUpdateStateMC();
	repaint();
    }

    public void setOptions()
    {
        panel.remove(hide);
        panel.remove(close);
	BufferedImage screen = ImageUtils.sceenComponent(panel);
	panel.removeAll();
	addFrameComp();
	panel.setOptions(screen);
	panel.add(updatepr);
	panel.add(cleanDir);
	panel.add(fullscreen);
	panel.add(memory);
	panel.add(options_close);
	if(Settings.useOffline)
	{
            panel.add(offline);
	}
	repaint();
    }        
    
    public void setLoading()
    {
        panel.remove(hide);
	panel.remove(close);
	BufferedImage screen = ImageUtils.sceenComponent(panel);
	panel.removeAll();
	addFrameComp();
	panel.setLoadingState(screen, "Выполнение...");
    }

    public void setError(String s)
    {
	panel.removeAll();
	addFrameComp();
	panel.setErrorState(s);
    }
}